# .attheme default theme generator

This tool generates source-code files for default themes for [`attheme-rs`] and
[`attheme-js`]. Basic usage:

[`attheme-rs`]: https://gitlab.com/snejugal/attheme-rs
[`attheme-js`]: https://github.com/snejugal/attheme-js

```bash
cargo run default_theme.attheme default.rs
```

This tool automatically infers the language and the name of the function
generating the theme from the output file name. A simplified output of the
command above:

```rust
use crate::Attheme;

/// Generates the Default theme.
pub fn default() -> Attheme {
    let mut theme = Attheme::with_capacity(619);

    theme.variables.insert(
        String::from("actionBarActionModeDefault"),
        [255, 255, 255, 255],
    );
    // ..code for all other variables
    theme
}
```

For themes with accent color, the usage is:

```bash
cargo run arctic.attheme arctic.rs --base-accent=3490EB
```

The base accent color is the first color of predefined accent colors in
Telegram UI. You don't have to take a screenshot of the Settings screen and use
a pipette tool to find out the color: you can find it in Telegram's source code
arond [this line] (which will shift with updates):

```java
themeInfo = new ThemeInfo();
themeInfo.name = "Blue";
// ..
themeInfo.setAccentColorOptions(new int[] {
    0xFF328ACF, // <==
    // ..
});
// ..
```

[this line]: https://github.com/DrKLO/Telegram/blob/master/TMessagesProj/src/main/java/org/telegram/ui/ActionBar/Theme.java#L2008

For old themes, the base accent color is likely one of these:

```yaml
mono: 328acf
dark: 3685fa
arctic: 3490eb
```

This tool will precalculate final colors as much as possible, generating
an output like:

```rust
use crate::Attheme;
use palette::{encoding, Hsv, Srgb};

/// Generates the Arctic theme.
pub fn arctic(accent: Hsv<encoding::Srgb, f64>) -> Attheme {
    let mut theme = Attheme::with_capacity(659);

    {
        let hsv = Hsv::new(
            accent.hue.to_positive_degrees() + -3.4263074307096,
            accent.saturation * 0.1530774627477,
            accent.value.mul_add(0.0439351293077, 0.1224608530998),
        );
        let rgb = Srgb::from(hsv).into_format::<u8>();

        theme.variables.insert(
            String::from("actionBarActionModeDefaultIcon"),
            [rgb.red, rgb.green, rgb.blue, 255],
        );
    }
    // ..
    theme
}
```

Make sure that the `attheme` crate, that this tool is being compiled with, has
an updated version of `default_themes::default`.

Note that this tool runs a formatter (`rustfmt` for Rust and `prettier` for
TypeScript) on the generated code. Make sure that they're discoverable with
`$PATH`, otherwise the generated code will be incredibly ugly.
