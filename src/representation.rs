use crate::default_theme::DefaultTheme;
use inflector::Inflector;
use palette::{encoding::Srgb, Hsv};
use std::path::Path;

mod rust;
mod typescript;

pub fn generate(
    default_theme: DefaultTheme,
    accent: Option<Hsv<Srgb, f64>>,
    output: &Path,
) -> Result<String, &'static str> {
    let name = output
        .file_stem()
        .ok_or("No name in output")?
        .to_str()
        .ok_or("Invalid name")?;
    let pretty_name = name.to_title_case();

    match output.extension() {
        Some(x) if x == "rs" => {
            Ok(rust::rust(&pretty_name, name, default_theme, accent))
        }
        Some(x) if x == "ts" => Ok(typescript::typescript(
            &pretty_name,
            if name == "default" {
                "defaultTheme"
            } else {
                name
            },
            default_theme,
            accent,
        )),
        _ => Err("Unknown output file type"),
    }
}
