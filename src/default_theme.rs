use attheme::{default_themes, Attheme};
use indexmap::IndexMap;
use palette::{encoding, Hsv, Hsva, Srgba};

const ACCENT_BLACKLIST: [&str; 16] = [
    "avatar_backgroundRed",
    "avatar_backgroundOrange",
    "avatar_backgroundViolet",
    "avatar_backgroundGreen",
    "avatar_backgroundCyan",
    "avatar_backgroundBlue",
    "avatar_backgroundPink",
    "avatar_nameInMessageRed",
    "avatar_nameInMessageOrange",
    "avatar_nameInMessageViolet",
    "avatar_nameInMessageGreen",
    "avatar_nameInMessageCyan",
    "avatar_nameInMessageBlue",
    "avatar_nameInMessagePink",
    "chat_attachFileBackground",
    "chat_attachGalleryBackground",
];

pub enum Color {
    Accent(Hsva<encoding::Srgb, f64>),
    Color(Srgba<u8>),
}

pub struct DefaultTheme {
    pub variables: IndexMap<String, Color>,
}

pub fn generate(
    mut theme: Attheme,
    accent: Option<Hsv<encoding::Srgb, f64>>,
) -> DefaultTheme {
    let mut variables: IndexMap<_, _> = if let Some(accent) = accent {
        theme.fallback_to_self(attheme::FALLBACKS);
        theme.fallback_to_other(default_themes::default());
        theme
            .variables
            .into_iter()
            .map(|(variable, color)| {
                let hsv = Hsva::from(color.into_format::<f64, _>());

                if ACCENT_BLACKLIST.contains(&variable.as_str())
                    || (hsv.hue.to_degrees() - accent.hue.to_degrees()).abs()
                        > 30.0
                {
                    (variable, Color::Color(color))
                } else {
                    (variable, Color::Accent(hsv))
                }
            })
            .collect()
    } else {
        theme
            .variables
            .into_iter()
            .map(|(variable, color)| (variable, Color::Color(color)))
            .collect()
    };

    variables.sort_keys();

    DefaultTheme { variables }
}
