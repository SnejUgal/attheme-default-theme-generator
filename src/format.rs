use std::{ffi::OsStr, path::Path};

pub fn format(output: &Path) {
    match output.extension() {
        Some(x) if x == "rs" => {
            let _ = std::process::Command::new("rustfmt")
                .args(&[
                    OsStr::new("--edition"),
                    OsStr::new("2018"),
                    output.as_os_str(),
                ])
                .output();
        }
        Some(x) if x == "ts" => {
            let _ = std::process::Command::new("prettier")
                .args(&[OsStr::new("--write"), output.as_os_str()])
                .output();
        }
        _ => unreachable!(),
    }
}
