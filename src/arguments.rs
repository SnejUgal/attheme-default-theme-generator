use palette::{encoding, Hsv, Srgb};
use std::{num::ParseIntError, path::PathBuf};
use structopt::StructOpt;

#[derive(StructOpt)]
pub struct Arguments {
    pub input: PathBuf,
    pub output: PathBuf,
    #[structopt(long, parse(try_from_str = parse_color))]
    pub base_accent: Option<Hsv<encoding::Srgb, f64>>,
}

fn parse_color(
    string: &str,
) -> Result<Hsv<encoding::Srgb, f64>, ParseIntError> {
    let numeric = u32::from_str_radix(string, 16)?;

    let blue = (numeric & 0xff) as u8;
    let green = ((numeric >> 8) & 0xff) as u8;
    let red = ((numeric >> 16) & 0xff) as u8;

    let rgb = Srgb::from_components((red, green, blue)).into_format();
    Ok(Hsv::from(rgb))
}

pub fn parse() -> Arguments {
    Arguments::from_args()
}
