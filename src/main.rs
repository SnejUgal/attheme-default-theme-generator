use attheme::Attheme;
use std::{error::Error, fs, io, path::Path};

mod arguments;
mod default_theme;
mod format;
mod representation;

fn read_theme(path: &Path) -> Result<Attheme, io::Error> {
    let contents = fs::read(path)?;
    Ok(Attheme::from_bytes(&contents[..]))
}

fn main() -> Result<(), Box<dyn Error>> {
    let arguments = arguments::parse();
    let input_theme = read_theme(&arguments.input)?;

    let default_theme =
        default_theme::generate(input_theme, arguments.base_accent);
    let representation = representation::generate(
        default_theme,
        arguments.base_accent,
        &arguments.output,
    )?;

    fs::write(&arguments.output, representation)?;
    format::format(&arguments.output);

    Ok(())
}
